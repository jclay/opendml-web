---
layout: default
---

<!-- <section-content id="full-width">
    <li><p>
        OpenDML allows for high performance CPU and
        GPU simulations of complex deformable bodies using a spring mass system. With our GPU implementation,
        we exceed state-of-the-art results, achieving over 1 billion springs per second. This performance enables
        simulation of increasingly large structures with consumer graphics cards in realtime.  
    </p></li>
</section-content> -->


<img src="{{ "/assets/img/opendml_demo.jpg" | relative_url }}">

<section>
    <section-title>Software</section-title>
    <section-content style="display:flex;">
        <li>
            <p><a href="#">Download OpenDML v1.0</a></p>
        </li>
        <li>
            <p><a href="#">Source code</a></p>
        </li>
        <li>
            <p><a href="#">Bug reports</a></p>
        </li>
    </section-content>
</section>

<section>
    <section-title>Documentation</section-title>
    <section-content style="display:flex;">
        <li>
            <p><a href="#">Getting Started</a></p>
        </li>
        <li>
            <p><a href="#">DML Reference</a></p>
        </li>
        <li>
            <p><a href="#">Paper</a></p>
        </li>
        <li>
            <p><a href="#">Developer Docs</a></p>
        </li>
    </section-content>
</section>
