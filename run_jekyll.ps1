$JEKYLL_VERSION=3.7

$site_path = Join-Path $PWD site

docker run --rm `
    -p 3000:4000 `
    -p 35729:35729 `
    --volume="$PWD`:/srv/jekyll" `
    --volume="$PWD/site/vendor/bundle:/usr/local/bundle" `
    -it jekyll/jekyll:$JEKYLL_VERSION `
    /bin/bash -c "cd site && jekyll serve --watch --livereload"